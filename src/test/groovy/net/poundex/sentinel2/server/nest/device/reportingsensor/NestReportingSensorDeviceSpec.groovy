package net.poundex.sentinel2.server.nest.device.reportingsensor


import com.launchdarkly.eventsource.EventSource
import net.poundex.sentinel2.server.device.port.DevicePort
import net.poundex.sentinel2.server.device.port.PortEvents
import net.poundex.sentinel2.server.env.value.NumericValue
import net.poundex.sentinel2.server.nest.thermostat.NestCredentials
import net.poundex.sentinel2.server.nest.thermostat.NestThermostat
import spock.lang.Specification
import spock.lang.Subject

class NestReportingSensorDeviceSpec extends Specification {
	
	NestThermostat thermostat = NestThermostat.builder().name("nest0").build()
	NestReportingSensorEventSourceFactory eventSourceFactory = Mock()
	NestCredentials nestCredentials = new NestCredentials("nest-device-id", "access-token")
	EventSource eventSource = Mock()
	PortEvents portEvents = Mock()
	
	@Subject
	NestReportingSensorDevice device = 
			new NestReportingSensorDeviceFactory(eventSourceFactory, portEvents)
					.createDevice(thermostat, nestCredentials)
	
	void "Creates and starts event source client"() {
		when:
		device.start()
		
		then:
		1 * eventSourceFactory.createClient(
				thermostat, device, nestCredentials) >> eventSource
		1 * eventSource.start()
	}
	
	void "Publishes port values for temperature and humidity on new report"() {
		given:
		PortEvents.PortValuePublishedEvent<NumericValue> expectedTemp = 
				new PortEvents.PortValuePublishedEvent<>(
						DevicePort.simple("nest://nest0/reportingsensor/temperature".toURI()), 
						new NumericValue(22.7))
		
		PortEvents.PortValuePublishedEvent<NumericValue> expectedHumid =
				new PortEvents.PortValuePublishedEvent<>(
						DevicePort.simple("nest://nest0/reportingsensor/humidity".toURI()), 
						new NumericValue(47))
		
		when:
		device.onSensorReport(new NestReportingSensorReport(new NestReportingSensorReport.Data(
				47, "en", "C", 22.7)))
		
		then:
		1 * portEvents.portValuePublished(expectedTemp)
		1 * portEvents.portValuePublished(expectedHumid)
	}
}
