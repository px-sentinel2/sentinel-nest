package net.poundex.sentinel2.server.nest.thermostat


import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.springframework.core.env.Environment
import org.springframework.http.HttpHeaders
import spock.lang.Specification
import spock.lang.Subject

class NestCloudServiceSpec extends Specification {
	
	private static final String NT_NAME = "thermostat-name"
	private static final String NT_DEVICE_ID = "nest-device-id"
	private static final String ACCESS_TOKEN = "deviceaccess-token"
	private static final String SERVICE_URL = "https://some-service-url"
	
	Environment springEnvironment = Stub() {
		getRequiredProperty("sentinel.nest.thermostat-name.deviceId") >> NT_DEVICE_ID
		getRequiredProperty("sentinel.nest.thermostat-name.accessToken") >> ACCESS_TOKEN
	}
	
	@Subject
	NestCloudService nestServiceUrlService

	MockWebServer mockWebServer = new MockWebServer()

	void setup() {
		mockWebServer.start()
		nestServiceUrlService = 
				new NestCloudService("http://localhost:${mockWebServer.port}/%s", springEnvironment)
	}

	void cleanup() {
		mockWebServer.shutdown()
	}

	void "Fetches new service URL for thermostat"() {
		setup:
		mockWebServer.enqueue(new MockResponse().setResponseCode(307)
				.addHeader(HttpHeaders.LOCATION, SERVICE_URL))

		when:
		String url = nestServiceUrlService.getServiceUrl(NT_NAME).get()
		
		then:
		with(mockWebServer.takeRequest()) {
			getHeader(HttpHeaders.AUTHORIZATION) == "Bearer ${ACCESS_TOKEN}"
			getRequestUrl().encodedPath() == "/${NT_DEVICE_ID}"
		}
		
		and:
		url == SERVICE_URL
	}

	void "Returns empty optional when no new service url available"() {
		setup:
		mockWebServer.enqueue(new MockResponse().setResponseCode(500))

		when:
		Optional<String> url = nestServiceUrlService.getServiceUrl(NT_NAME)
		
		then:
		url.empty
	}
	
}
