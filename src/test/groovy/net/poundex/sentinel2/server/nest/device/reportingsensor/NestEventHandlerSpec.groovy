package net.poundex.sentinel2.server.nest.device.reportingsensor

import com.fasterxml.jackson.databind.ObjectMapper
import com.launchdarkly.eventsource.MessageEvent
import spock.lang.Specification
import spock.lang.Subject

class NestEventHandlerSpec extends Specification {
	
	NestReportingSensorListener listener = Mock()
	
	@Subject
	NestEventHandler nestEventHandler = new NestEventHandler(listener, new ObjectMapper())
	
	void "Publishes sensor report on new event"() {
		given:
		NestReportingSensorReport expected = 
				new NestReportingSensorReport(new NestReportingSensorReport.Data(
						40, "en", "C", 22.7))
		
		when:
		nestEventHandler.onMessage("put", new MessageEvent("""\
{
	"data": {
		"humidity": 40,
		"locale": "en",
		"temperature_scale": "C",
		"ambient_temperature_c": 22.7
	}
}
"""))
		
		then:
		1 * listener.onSensorReport(expected)
	}
}
