package net.poundex.sentinel2.server.nest.device.reportingsensor


import net.poundex.sentinel2.server.device.port.DevicePort
import net.poundex.sentinel2.server.nest.thermostat.NestCredentials
import net.poundex.sentinel2.server.nest.thermostat.NestThermostat
import spock.lang.Specification
import spock.lang.Subject

class NestReportingSensorDeviceFactorySpec extends Specification {

	NestCredentials credentials = new NestCredentials("nest-device-id", "access-token")

	@Subject
	NestReportingSensorDeviceFactory factory =
			new NestReportingSensorDeviceFactory(null, null)
	
	void "Nest Reporting Sensor Devices have correct ports"() {
		given:
		NestThermostat nestThermostat = NestThermostat.builder().name("nest0").build()

		when:
		NestReportingSensorDevice device = factory.createDevice(nestThermostat, credentials)

		then:
		device.humidityPort.portId == "nest://nest0/reportingsensor/humidity".toURI()
		device.temperaturePort.portId == "nest://nest0/reportingsensor/temperature".toURI()
	}
}
