package net.poundex.sentinel2.server.nest.device

import net.poundex.sentinel2.server.device.DriverEvents
import net.poundex.sentinel2.server.nest.device.reportingsensor.NestReportingSensorDevice
import net.poundex.sentinel2.server.nest.device.reportingsensor.NestReportingSensorDeviceFactory
import net.poundex.sentinel2.server.nest.thermostat.NestCloudService
import net.poundex.sentinel2.server.nest.thermostat.NestCredentials
import net.poundex.sentinel2.server.nest.thermostat.NestThermostat
import net.poundex.sentinel2.server.nest.thermostat.NestThermostatRepository
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

class NestDeviceServiceSpec extends Specification {
	
	private static final URI NT_DEVICE_ID = URI.create("nest://nest0/self")
	
	DriverEvents driverEvents = Mock()
	NestThermostatRepository thermostatRepository = Stub()
	NestCredentials credentials = new NestCredentials("nest-device-id", "access-token")
	NestCloudService nestService = Stub() {
		getCredentials("nest0") >> credentials
	}
	NestReportingSensorDeviceFactory reportingSensorDeviceFactory = Stub()
	NestThermostat thermostat = NestThermostat.builder().name("nest0").build();
	
	@Subject
	NestDeviceService nestDeviceService = new NestDeviceService(
			driverEvents, thermostatRepository, reportingSensorDeviceFactory, nestService)
	
	void "Registers reporting sensor device"() {
		given:
		thermostatRepository.findAll() >> Flux.just(thermostat)
		NestReportingSensorDevice expected = Mock(NestReportingSensorDevice) {
			getDeviceId() >> NT_DEVICE_ID.resolve(NestReportingSensorDevice.DEVICE_PATH)
		}
		reportingSensorDeviceFactory.createDevice(thermostat, credentials) >> expected
		
		when:
		nestDeviceService.start()
		
		then:
		1 * driverEvents.deviceAdded(expected)
		1 * expected.start() >> expected
	}
}
