package net.poundex.sentinel2.server.nest.device;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.device.Driver;
import net.poundex.sentinel2.server.device.DriverEvents;
import net.poundex.sentinel2.server.nest.device.reportingsensor.NestReportingSensorDeviceFactory;
import net.poundex.sentinel2.server.nest.thermostat.NestCloudService;
import net.poundex.sentinel2.server.nest.thermostat.NestThermostat;
import net.poundex.sentinel2.server.nest.thermostat.NestThermostatRepository;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
class NestDeviceService implements Driver {
	
	private final DriverEvents driverEvents;
	private final NestThermostatRepository thermostatRepository;
	private final NestReportingSensorDeviceFactory reportingSensorDeviceFactory;
	private final NestCloudService nestService;

	@Override
	public String getName() {
		return "nest";
	}

	@Override
	public void start() {
		// TODO: block
		thermostatRepository.findAll().collectList().block().forEach(this::startThermostat);
	}

	private void startThermostat(NestThermostat thermostat) {
		log.info("Configuring Nest Thermostat {}", thermostat.getName());
		driverEvents.deviceAdded(reportingSensorDeviceFactory.createDevice(thermostat, 
				nestService.getCredentials(thermostat.getName())).start());
	}
}
