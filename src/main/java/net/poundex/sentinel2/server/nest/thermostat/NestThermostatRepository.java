package net.poundex.sentinel2.server.nest.thermostat;

import net.poundex.sentinel2.ReadOnlyRepository;
import reactor.core.publisher.Mono;

public interface NestThermostatRepository extends ReadOnlyRepository<NestThermostat> {
	Mono<NestThermostat> create(String name);
}
