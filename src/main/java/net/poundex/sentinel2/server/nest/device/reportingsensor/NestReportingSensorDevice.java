package net.poundex.sentinel2.server.nest.device.reportingsensor;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.device.Device;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.PortEvents;
import net.poundex.sentinel2.server.env.value.NumericValue;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.nest.thermostat.NestCredentials;
import net.poundex.sentinel2.server.nest.thermostat.NestThermostat;

import java.net.URI;
import java.util.Optional;

import static io.vavr.API.*;

@Data
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Slf4j
public class NestReportingSensorDevice implements Device, NestReportingSensorListener {
	
	public static final String DEVICE_PATH = "reportingsensor/self";

	private final URI deviceId;
	private final DevicePort<NumericValue> temperaturePort;
	private final DevicePort<NumericValue> humidityPort;
	private final NestThermostat thermostat;
	private final NestReportingSensorEventSourceFactory eventSourceFactory;
	private final NestCredentials nestCredentials;
	private final PortEvents portEvents;

	@SuppressWarnings("unchecked")
	@Override
	public <VT extends Value<VT>> Optional<DevicePort<VT>> getPort(URI path) {
		return Match(path).option(
				Case($(DevicePort.TEMPERATURE), temperaturePort), 
				Case($(DevicePort.HUMIDITY), humidityPort))
				.map(dp -> (DevicePort<VT>) dp)
				.toJavaOptional();
	}
	
	public NestReportingSensorDevice start() {
		log.debug("Starting event client for {}", deviceId);
		eventSourceFactory
				.createClient(thermostat, this, nestCredentials)
				.start();
		return this;
	}

	@Override
	public void onSensorReport(NestReportingSensorReport report) {
		portEvents.portValuePublished(new PortEvents.PortValuePublishedEvent<>(
				temperaturePort,
				new NumericValue(report.data().ambient_temperature_c())));

		portEvents.portValuePublished(new PortEvents.PortValuePublishedEvent<>(
				humidityPort,
				new NumericValue(report.data().humidity())));
	}
}
