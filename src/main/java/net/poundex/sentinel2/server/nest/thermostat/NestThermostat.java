package net.poundex.sentinel2.server.nest.thermostat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.Hardware;

import java.net.URI;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
public class NestThermostat implements SentinelObject, Hardware {
	
	private final String id;
	private final String name;
	private final String serviceUrl;

	@Override
	public URI getDeviceId() {
		return URI.create(String.format("nest://%s/self", name));
	}
}
