package net.poundex.sentinel2.server.nest.device.reportingsensor;

import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.PortEvents;
import net.poundex.sentinel2.server.nest.thermostat.NestCredentials;
import net.poundex.sentinel2.server.nest.thermostat.NestThermostat;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
@RequiredArgsConstructor
public class NestReportingSensorDeviceFactory {
	
	private final NestReportingSensorEventSourceFactory eventSourceFactory;
	private final PortEvents portEvents;
	
	public NestReportingSensorDevice createDevice(NestThermostat nestThermostat, NestCredentials nestCredentials) {
		URI deviceId = nestThermostat.getDeviceId().resolve(NestReportingSensorDevice.DEVICE_PATH);
		return new NestReportingSensorDevice(deviceId,
				DevicePort.simple(deviceId.resolve(DevicePort.TEMPERATURE)),
				DevicePort.simple(deviceId.resolve(DevicePort.HUMIDITY)),
				nestThermostat,
				eventSourceFactory, 
				nestCredentials,
				portEvents);
	}

}
