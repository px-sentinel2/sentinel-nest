package net.poundex.sentinel2.server.nest.device.reportingsensor;

record NestReportingSensorReport(Data data) {
	record Data(
			int humidity,
			String locale,
			String temperature_scale,
			double ambient_temperature_c) { }
}

