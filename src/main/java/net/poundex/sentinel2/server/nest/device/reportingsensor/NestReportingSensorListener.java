package net.poundex.sentinel2.server.nest.device.reportingsensor;

interface NestReportingSensorListener {
	void onSensorReport(NestReportingSensorReport report);
}
