package net.poundex.sentinel2.server.nest.thermostat;

public record NestCredentials(String deviceId, String accessToken) {
}
