package net.poundex.sentinel2.server.nest.device.reportingsensor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.launchdarkly.eventsource.EventSource;
import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.server.nest.thermostat.NestCredentials;
import net.poundex.sentinel2.server.nest.thermostat.NestThermostat;
import okhttp3.Headers;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Map;

@Component
@RequiredArgsConstructor
class NestReportingSensorEventSourceFactory {
	
	private final ObjectMapper objectMapper;
	
	public EventSource createClient(
			NestThermostat nestThermostat, 
			NestReportingSensorDevice reportingSensorDevice, 
			NestCredentials credentials) {
		return new EventSource.Builder(new NestEventHandler(reportingSensorDevice, objectMapper), URI.create(nestThermostat.getServiceUrl()))
				.headers(Headers.of(Map.of(
						HttpHeaders.AUTHORIZATION,
						String.format("Bearer %s", credentials.accessToken()))))
				.build();
	}
}
