package net.poundex.sentinel2.server.nest.device.reportingsensor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.MessageEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
class NestEventHandler implements EventHandler {
	
	private final NestReportingSensorListener listener;
	private final ObjectMapper objectMapper;

	@Override
	public void onMessage(String event, MessageEvent messageEvent) throws Exception {
		if( ! event.equals("keep-alive"))
			log.debug("Nest Event: {} {}", event, messageEvent);
		
		if ( ! event.equals("put"))
			return;

		listener.onSensorReport(
				objectMapper.readValue(messageEvent.getData(), NestReportingSensorReport.class));
	}

	@Override
	public void onError(Throwable t) {
		log.error("Error in Nest Reporting Sensor Device Event Handler", t);
	}

	@Override
	public void onOpen() throws Exception { }

	@Override
	public void onClosed() throws Exception { }

	@Override
	public void onComment(String comment) throws Exception { }
}
