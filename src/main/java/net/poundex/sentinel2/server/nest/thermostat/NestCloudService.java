package net.poundex.sentinel2.server.nest.thermostat;

import io.vavr.control.Try;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Stream;

@Service
public class NestCloudService {
	
	private final Environment springEnvironment;
	private final String apiUrl;

	@Autowired
	public NestCloudService(Environment springEnvironment) {
		this("https://developer-api.nest.com/devices/thermostats/%s", springEnvironment);
	}

	NestCloudService(String apiUrl, Environment springEnvironment) {
		this.springEnvironment = springEnvironment;
		this.apiUrl = apiUrl;
	}

	public Optional<String> getServiceUrl(String name) {
		return Stream.of(Try.of(() ->
						new OkHttpClient.Builder().followRedirects(false)
								.build().newCall(new Request.Builder()
										.url(String.format(apiUrl, getCredentials(name).deviceId()))
										.addHeader(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", getCredentials(name).accessToken()))
										.addHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
										.build()).execute()).get())
				.filter(x -> x.code() == 307)
				.map(r -> r.header("Location"))
				.findFirst();
	}

	public NestCredentials getCredentials(String name) {
		return new NestCredentials(
				springEnvironment.getRequiredProperty(String.format("sentinel.nest.%s.deviceId", name)),
				springEnvironment.getRequiredProperty(String.format("sentinel.nest.%s.accessToken", name)));
	}
}
